#include "ofApp.h"
#include <iostream>

//--------------------------------------------------------------
void ofApp::setup(){

    ofEnableDepthTest();
    mesh.setMode(OF_PRIMITIVE_POINTS);
//    mesh.setMode(OF_PRIMITIVE_TRIANGLES);
    mesh.enableColors();
    mesh.enableIndices();
    
    ofSetBackgroundColor(180);

    box.set(10, 10, 10);
    cam.setPosition(100, 100, 100);
    
    d = 0;
    a = 0;
    
    numRows = 40;
    numCols = 40;
    
    for (int c = 1; c < numCols/2; c++){
        for (int r = 1; r < numRows/2; r++){
            ofVec3f v1(c * 10, r * 10, 0);
            mesh.addVertex(v1);
            mesh.addColor(ofFloatColor(0, 0, 0));  // add a color at the vertices
        }
    }
    
    mesh.addIndex(0);
    mesh.addIndex(1);
    mesh.addIndex(2);
    
    // Set up triangles' indices
    // only loop to -1 so they don't connect back to the beginning of the row
    for(int x = 0; x < numCols - 1; x++) {
        for(int y = 0; y < numRows - 1; y++) {
            int topLeft     = x   + numCols * y;
            int bottomLeft  = x+1 + numCols * y;
            int topRight    = x   + numCols * (y+1);
            int bottomRight = x+1 + numCols * (y+1);

            mesh.addTriangle( topLeft, bottomLeft, topRight );
            mesh.addTriangle( bottomLeft, topRight, bottomRight );

        }
    }
    
}

//--------------------------------------------------------------
void ofApp::update(){


}

//--------------------------------------------------------------
void ofApp::draw(){

    cam.begin();
    
    cam.panDeg(a);
    cam.dolly(d);
    
    mesh.draw();
    mesh.drawWireframe();

    box.setPosition(100, 100, 0);
    ofSetColor(250, 160, 160);
    box.draw();
    
    cam.end();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    
    if (key == 'w'){
        
        d -= 0.3;
    }
    if (key == 'a'){
        
        d += 0.3;
    }
    if (key == 's'){

        a += 0.1;
    }
    if (key == 'd'){

        a -= 0.1;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
    if (key == 'w'){
        d = 0;
    }
    if (key == 'a'){
        d = 0;
    }
    if (key == 's'){
        a = 0;
    }
    if (key == 'd'){
        a = 0;
    }

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
